// to compile this file, type:
// g++ randomdesktopemulator.c -o randomdesktopemulator -O3 -s -lGL -lglut -lpthread

// Dawn Operating System emulator (SUBLEQ) for random opensource operating systems (little endian)
// by Geri
// http://gerigeri.uw.hu/DawnOS/index.html
// MIT license / BSD license (whatever you like)


// READ THIS NOTICE CAREFULLY BEFORE YOU COMPILE, REFACTOR, OR SCANVEGE THIS EXAMPLE CODE
// BECAUSE ITS SHIT

// compiles om linux, windows, bsd, whatever


// ******************************************
// this code is not actually optimized or cleaned
// its barely readable or makes sense
// it will compile in c++ compiler (g++)
// its just quickly shoveled together, full of quirks
// graphics will fail on a lot of phones, no idea why
// if you want to write a proper emulator
// i not suggest to use this code
// or at least you should rewrite properly
// ******************************************
// please not that dawn needs a big endian 64 bit subleq cpu
// but this is meant for little endian cpus
// you need to throw out bswaps if you compiling it on big endian computer
// ******************************************
// it isnt as complex as it maybe looks like for the first,
// its only long because of the host system api.
// 90% of the code is actually to deal the cancerous shit of android/linux
// (just reading out the cpu name is longer than actually emulating the subleq cpu for dawn itself)
// the rest 10% is the actual emulation of cpu and the hardwares
// ******************************************
// it emulates maximum 12 cpu cores
// it emulates graphics output
// disk
// keyboard
// touchscreen.
// network file upload and download
// ******************************************
// it NOT emulates webcamera, sound, accelerometer, satelite navigation
// ******************************************
// this source code is only opened by request of people to see
// how they actually should do a full dawn system emulation
// it have nothing valuable, so i agreed to open
// it uses glut for output, keyboard, and touschreen
// ******************************************
// (glut for graphics and input for now, as its
// usually available everywhere widely, and not
// because its good - you should write proper platform management on varios platforms!)
// ******************************************
// sorry for hungarian language, i didnt meant
// to release it at all, i am lazy to redo everything properly
// please do your own emulator if you dislike
// please do not even contact me just to point out
// or fix bugs in this code, I DO NOT CARE
// ******************************************

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <ctype.h>
#include <pthread.h>


unsigned char *RAM=NULL; // memory for dawn
long long RESX=1024;
long long RESY=512;

// DAWN os memory locations

#define PROCESSZOROK_ADAT 334364672
#define HARDVEREKKEZD 268435456
#define HARDVEREKVEGE 335544320
#define GRAFIKUSKEP 268435456
#define LEMEZ_KERNEL_TERULET 268435456
#define IDO_ADAT 335544304
#define HANGKARTYA_ADAT 335544116
#define BILLENTYUZET_ADAT 335542256
#define EGER_ADAT 335542176
#define FELBONTAS_ADAT 335540096
#define MENNYI_ORAJEL_PROCIAKTIVALAS 334364664
#define PROCESSZORNEVE 335413288
#define LEMEZEK_ADAT 334361440
#define HALOZATHATOKOR 335413592
#define HALOZATFOGAD_ADATOK 335413400
#define HALOZATKULD_ADATOK 335413336

#define bswap64 __builtin_bswap64
#define bswap32 __builtin_bswap32


// thread creation (its just actually pthread, but without syntax cancer)
// you dont need threads, if you dont want SMP.
int specialis_stackmeret=0;
pthread_attr_t attr;
void threadstackmeretmax(int meret){
	size_t stacksize=meret;
	pthread_attr_init(&attr);
	pthread_attr_getstacksize (&attr, &stacksize);
	stacksize=meret;
	pthread_attr_setstacksize (&attr, stacksize);
	specialis_stackmeret=1;
}
pthread_t *sz2w32e=NULL;
void thread_inicializal(){
	if(!sz2w32e){
		sz2w32e=(pthread_t *)malloc(sizeof(pthread_t)*4*1024);
		if(!sz2w32e) sz2w32e=(pthread_t *)malloc(sizeof(pthread_t)*4*1024);
		memset(sz2w32e, 0, sizeof(pthread_t)*4*1024);
	}
}
int STARTTHREAD(int n, void *(*procd)(void *)){
	if((n<1) || (n>1023)){
		return 0;
	}
	thread_inicializal();
	n*=4;
	if(!specialis_stackmeret){
		long t=0;pthread_create(&sz2w32e[n], NULL, procd, (void*)t);
	}else{
		size_t stacksize;
		pthread_attr_getstacksize (&attr, &stacksize);
		//printf("%d\n", stacksize);
		long t=0;pthread_create(&sz2w32e[n], &attr, procd, (void*)t);
	}
	return 1;
}


// mouse craps to deal with glut
int balegergomb=0;
int jobbegergomb=0;
int kozepsoegergomb=0;
int egergorgofel=0;
int egergorgole=0;

float valodi_egerkurzor_HelyeX=0.5;
float valodi_egerkurzor_HelyeY=0.5;

float elozoegerxtmp=0.5;
float elozoegerytmp=0.5;


// opengl cancer
#include "GL/gl.h"
#include "GL/glut.h"

int textbindelve=0;
GLuint gl_rgb_tex=0;

void render32(void* tex, int x, int y){

	RESX=glutGet(GLUT_WINDOW_WIDTH);
	RESY=glutGet(GLUT_WINDOW_HEIGHT);
	if(RESX<1) RESX=512;
	if(RESY<1) RESY=256;
	glViewport(0, 0, RESX, RESY);

	// update screen size for OS
	volatile long long *vga=(long long *)(&RAM[FELBONTAS_ADAT]);
	vga[0]=bswap64(RESX); // in case the os want different resolution... it wont get it, coz why, we forcing the os to the window screen
	vga[1]=bswap64(RESY);
	vga[2]=bswap64(32); // 32 bit

	 // render
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glClearColor(0.0, 0.0, 0.0, 1.0);
  
	if(!gl_rgb_tex){
		textbindelve=1;
		glEnable(GL_TEXTURE_2D);
		glGenTextures(1, &gl_rgb_tex);
		glBindTexture(GL_TEXTURE_2D, gl_rgb_tex);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glPixelStorei(GL_PACK_ALIGNMENT, 1);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	}

	if(tex) glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, x, y, 0, GL_RGBA, GL_UNSIGNED_BYTE, (unsigned char*)tex);

	float v[12] = {-1.0, -1.0, -1.0, 1.0, 1.0, -1.0, -1.0, 1.0, 1.0, 1.0, 1.0, -1.0};
	float t[12] = {0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0};

	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glVertexPointer(2, GL_FLOAT, 0, v);
	glEnableClientState(GL_VERTEX_ARRAY);
	glTexCoordPointer(2, GL_FLOAT, 0, t);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glDrawArrays(GL_TRIANGLES, 0, 8);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glVertexPointer(2, GL_FLOAT, 0, NULL);
	glTexCoordPointer(2, GL_FLOAT, 0, NULL);
	// glFlush();
	glutSwapBuffers();

  
	
}

// get number of cpu cores
int cpu_magok(){
	int p=(int)sysconf(_SC_NPROCESSORS_CONF);
	if(p==4) p*=2; // a 8 magos snaptaknyok miatt
	if(p==6) p*=2; // a 12 magos snaptaknyok miatt
	return p;
}


// get linux time (linux cancer)
long double elsojelenlegiidolinux=0;
int jelenlegiidovoltmarlekerdezvelinux=0;
#define most_ido jelenlegi_ido
long long int kezdoidokivonlinuxhack=0;
long double jelenlegi_ido_s(){
	timeval tv;
	gettimeofday(&tv, NULL);
	if(!jelenlegiidovoltmarlekerdezvelinux){
		kezdoidokivonlinuxhack=tv.tv_sec;
	}
	long double prett_r=((tv.tv_sec-kezdoidokivonlinuxhack)+(tv.tv_usec / 1000000.0));
	if(!jelenlegiidovoltmarlekerdezvelinux){
		jelenlegiidovoltmarlekerdezvelinux=1;
		elsojelenlegiidolinux=(long double)((long double)prett_r);
	}
	return ((long double)((long double)prett_r)-elsojelenlegiidolinux);
}
long double elozojelenlegiidobiztonsagi=0; // óraátállítás esetén is rendesen mér
long double elozojelenlegiidobiztonsagi_hozzaad=0;
long double jelenlegi_ido(){
	double w=jelenlegi_ido_s();
	if(w<elozojelenlegiidobiztonsagi){
		elozojelenlegiidobiztonsagi_hozzaad=elozojelenlegiidobiztonsagi-w;
		elozojelenlegiidobiztonsagi=w+0.02;
		#ifdef __linux__
			usleep(40000);
		#else
			Sleep(40);
		#endif
	}
	w+=elozojelenlegiidobiztonsagi_hozzaad;
	return w;
}


// disk drive emulation (you can delete this if you want no disks)
void lemezciklus(int d){
	FILE *lemez=NULL;
	volatile unsigned long long * RAMW=(unsigned long long*)&RAM[LEMEZEK_ADAT+(d*24)];
	long long fajlkezelesvanmostanaban=0;
	long long meret=0;
	if(d==0){
		#ifdef WIN32
		  lemez=fopen("..\\..\\disk0.bin", "r");
		  if(lemez){
			  fclose(lemez);
			  lemez=fopen("..\\..\\disk0.bin", "r+");
		  }
		  if(!lemez){
		    lemez=fopen("..\\..\\disk0.bin", "r");
		    if(lemez){
			  fclose(lemez);
			  lemez=fopen("..\\..\\disk0.bin", "r+");
		    }
		  }
		#else
		  lemez=fopen("../../disk0.bin", "r");
		  if(lemez){
			  fclose(lemez);
			  lemez=fopen("../../disk0.bin", "r+");
		  }
		  if(!lemez){
		    lemez=fopen("../../disk0.bin", "r");
		    if(lemez){
			  fclose(lemez);
			  lemez=fopen("../../disk0.bin", "r+");
		    }
		  }
		#endif
	}
	if(d==1){
		#ifdef WIN32
		  lemez=fopen("..\\..\\disk1.bin", "r");
		  if(lemez){
			  fclose(lemez);
			  lemez=fopen("..\\..\\disk1.bin", "r+");
		  }
		  if(!lemez){
		    lemez=fopen("..\\..\\disk1.bin", "r");
		    if(lemez){
			  fclose(lemez);
			  lemez=fopen("..\\..\\disk1.bin", "r+");
		    }
		  }
		#else
		  lemez=fopen("../../disk1.bin", "r");
		  if(lemez){
			  fclose(lemez);
			  lemez=fopen("../../disk1.bin", "r+");
		  }
		  if(!lemez){
		    lemez=fopen("../../disk1.bin", "r");
		    if(lemez){
			  fclose(lemez);
			  lemez=fopen("../../disk1.bin", "r+");
		    }
		  }
		#endif
	}
	if(lemez){
	    fseek(lemez, 0, SEEK_END);
	    meret=ftell(lemez);
	    // printf("%lld\n", meret);
	    fseek(lemez, 0, SEEK_SET);
	}
	while(1){
		{
			if(lemez){
				if(!RAMW[2]) RAMW[2]=bswap64(1);
			}
			
			if(fajlkezelesvanmostanaban>0) fajlkezelesvanmostanaban--;
			if(fajlkezelesvanmostanaban<=0) usleep(3000);
			// lemez
			// volatile unsigned long long * RAMW=(unsigned long long*)&RAM[LEMEZEK_ADAT+(0*24)];
			if(lemez){
				if(RAMW[2]==bswap64(2)){
					if(bswap64(RAMW[1])>=meret){
						RAMW[0]=bswap64(-1);
						RAMW[2]=bswap64(3);
						// printf("range hiba %lld\n", bswap64(RAMW[1]));
					}else{
						fseek(lemez, bswap64(RAMW[1]), SEEK_SET);
						long long r=fgetc(lemez);
						// printf("szerver oldal olvas %lld %lld %c\n", bswap64(RAMW[1]), r, r);
						if(r==-1) RAMW[0]=bswap64(-1); else RAMW[0]=bswap64((unsigned long long)(unsigned char)r);
						RAMW[2]=bswap64(3);
					}
					fajlkezelesvanmostanaban=999999;
				}
				if(RAMW[2]==bswap64(4)){
					if(bswap64(RAMW[1])>=meret){
						RAMW[0]=bswap64(-1);
						RAMW[2]=bswap64(5);
					}else{
						fseek(lemez, bswap64(RAMW[1]), SEEK_SET);
						// printf("szerver oldal ir %lld %lld\n", RAMW[1], RAMW[0]);
						long long r=fputc(bswap64(RAMW[0]), lemez);
						if(r==-1) RAMW[0]=bswap64(-1); else RAMW[0]=bswap64((unsigned long long)(unsigned char)r);
						RAMW[2]=bswap64(5);
					}
					fajlkezelesvanmostanaban=999999;
				}
			}
		}
	}
	// return 0;
}

void * lemezciklus2(void *threadid){lemezciklus(0);}
void * lemezciklus1(void *threadid){lemezciklus(1);}


// rendering, and exit watchdog
long long idonkentkirajzol=10000;
long long elozoframemutato=-1;
void munkaciklus(){
	idonkentkirajzol++;
	volatile long long *vga=(long long *)(&RAM[FELBONTAS_ADAT]);
	if((elozoframemutato!=bswap64(vga[4]))||(idonkentkirajzol>10)){
		elozoframemutato=bswap64(vga[4]); // this is  counter for vsync, adjusted by the os
		idonkentkirajzol=0;
		render32(&RAM[256*1024*1024], RESX, RESY); // if not failsafe, do it from the subleq loop of the 0th cpu sometimes.
	}

	volatile long long *cpu_adat=(long long *)(&RAM[PROCESSZOROK_ADAT]);
	if(cpu_adat[0]==bswap64(8)){
		exit(0);
	}
}


// get cpu name string ( https://s-media-cache-ak0.pinimg.com/originals/fa/29/34/fa2934d657ce31a93f74dc22c6a4347f.png ) *mental meltdown*
char cpunevoprhez[41];
#define strlen_ownimp strlen
void rtCPUID(){
	char cpunev[256];
	char cpugyarto[256];
	srand(time(NULL));

	#ifdef __linux__
		int pidem=getpid();
		char gpidd[1024];
		FILE * pidkolvas=NULL;
		// procinév
		cpunev[0]='\0';
		snprintf(gpidd, 900, "cat /proc/cpuinfo | grep \"model name\" | cut -b 14- > /tmp/528576CPUN.txt");
		system(gpidd);
		snprintf(gpidd, 920, "/tmp/528576CPUN.txt");
		pidkolvas=NULL;pidkolvas=fopen(gpidd, "r");

		if(!pidkolvas){
			cpunev[0]='\0';
			snprintf(gpidd, 900, "cat /proc/cpuinfo | grep \"Processor\" | cut -b 14- > /tmp/528576CPUN.txt");
			system(gpidd);
			snprintf(gpidd, 920, "/tmp/528576CPUN.txt");
			pidkolvas=NULL;pidkolvas=fopen(gpidd, "r");
		}

		if(!pidkolvas){
			pidkolvas=NULL;pidkolvas=fopen("/proc/cpuinfo", "r");
			if(!pidkolvas){
			}else{
				char tmpt[256];
				char masodlagostmp[256];
				int masodlagos=0;
				int proba=0;
				int megvannev=0;
				while(1){
					proba++;
					if(proba>10000) break;
					tmpt[0]='\0';
					char *a=fgets(tmpt, 255, pidkolvas);
					if(!a){
						break;
					}
					if(strstr(a, "Processor")||strstr(a, "processor")){
						masodlagos=1;
						snprintf(masodlagostmp, 255, "%s", &a[13]);
					}
					if(strstr(a, "model name")){
						snprintf(cpunev, 255, "%s", &a[13]);
						megvannev=1;
						break;
					}
				}
				fclose(pidkolvas);
				pidkolvas=NULL;
				if(!megvannev){
					if(masodlagos){
						strcpy(cpunev, masodlagostmp);
						megvannev=1;
					}
				}
				if(!megvannev){
				}
			}
		}else{
			if(fgets(cpunev, 255, pidkolvas)==NULL){
			}else{
			}
			fclose(pidkolvas);
		}
		remove(gpidd);
		// proci gyártója
		cpugyarto[0]='\0';
		snprintf(gpidd, 900, "cat /proc/cpuinfo | grep \"vendor_id\" | cut -b 13- > /tmp/128576CPUN.txt");
		system(gpidd);
		snprintf(gpidd, 920, "/tmp/128576CPUN.txt");
		pidkolvas=NULL;pidkolvas=fopen(gpidd, "r");
		if(!pidkolvas){
			cpugyarto[0]='\0';
			snprintf(gpidd, 900, "cat /proc/cpuinfo | grep \"Hardware\" | cut -b 14- > /tmp/128576CPUN.txt");
			system(gpidd);
			snprintf(gpidd, 920, "/tmp/128576CPUN.txt");
			pidkolvas=NULL;pidkolvas=fopen(gpidd, "r");
		}
		if(!pidkolvas){
			//if((diag>2)) printf("e_Int: ");if((diag>2)) printf("Az engine nem tudta megallapitani a processzor gyártóját.\n");
		}else{
			if(fgets(cpugyarto, 255, pidkolvas)==NULL){
				//if((diag>2)) printf("e_Int: ");if((diag>2)) printf("Az engine nem tudta megallapitani a processzor gyártóját.\n");
			}else{
			}
			fclose(pidkolvas);
		}
		remove(gpidd);
	#endif

	if(strlen(cpugyarto)<2) sprintf(cpugyarto, "Unknown");
	if(strlen(cpunev)<2) sprintf(cpunev, "Unknown");
		for(int unix_patch=0;unix_patch<256;unix_patch++){
		if(cpugyarto[unix_patch]=='\0') break;
		if(cpugyarto[unix_patch]=='\n'){
			cpugyarto[unix_patch]='\0';
			break;
		}
	}
	for(int unix_patch=0;unix_patch<256;unix_patch++){
		if(cpunev[unix_patch]=='\0') break;
		if(cpunev[unix_patch]=='\n'){
			cpunev[unix_patch]='\0';
			break;
		}
	}
	snprintf(cpunevoprhez, 41, "%s", cpunev);
	cpunevoprhez[40]=0;
	cpunevoprhez[41]=0;
}

// on x86-32 you can use this
//unsigned int emulalproc(unsigned int eip){
//	for(unsigned int w=0;w<4096;w++){
//		unsigned int * utasitaskezd=(unsigned int*)&RAM[eip];
//		unsigned int A=bswap32(utasitaskezd[1]);
//		unsigned int B=bswap32(utasitaskezd[3]);
//		long long B_ertek=bswap64(*(unsigned long long*)&RAM[B])-bswap64(*(unsigned long long*)&RAM[A]);
//		if(B_ertek<=0){
//		    eip=bswap32(utasitaskezd[5]);
//		}else{
//		    eip+=24;
//		}
//		*(long long*)&RAM[B]=bswap64(B_ertek);
//	}
//	return eip;
//}


// on x86-64 you can use this
unsigned int emulalproc(unsigned int eip){
	for(unsigned int w=0;w<4096;w++){
		unsigned long long * utasitaskezd=(unsigned long long*)&RAM[eip];
		unsigned long long A=bswap64(utasitaskezd[0]);
		unsigned long long B=bswap64(utasitaskezd[1]);
		long long B_ertek=bswap64(*(unsigned long long*)&RAM[B])-bswap64(*(unsigned long long*)&RAM[A]);
		if(B_ertek<=0){
		    eip=bswap64(utasitaskezd[2]);
		}else{
		    eip+=24;
		}
		*(long long*)&RAM[B]=bswap64(B_ertek);
	}
	return eip;
}

// actual subleq emulation on cancerous ARM
// this should be rewritten in armv7 assembly with the set BIG ENDIAN opcode for much bigger performance
//unsigned int emulalproc(unsigned int eip){
//	unsigned char * uRAM=(unsigned char*)RAM;
//	for(unsigned int w=0;w<4096;w++){
//		unsigned int * utasitaskezd=(unsigned int*)&RAM[eip]; // this supposed to be aligned by the dawn compiler or os itself, but the things can be different with thirdparty tools
//		// if(flog_entry>=logstart) fprintf(flog, "eip:%d\n", eip);fflush(flog);
//		unsigned int A=bswap32(utasitaskezd[1]);  // doing actually only 32 bit. makes no sense to do long long in a 32 bit arm binary. this limits the maximum ram to 3,9 gbyte.
//		unsigned int B=bswap32(utasitaskezd[3]);
//		// if(flog_entry>=logstart) fprintf(flog, "%d %d\n", A, B);fflush(flog);
//		unsigned long long a_szam;
//		unsigned long long b_szam;
//		if(A&7){ // unaligned memory access detected. not needed on x86 or armv7+
//			a_szam=uRAM[A]<<8;
//			a_szam|=uRAM[A+1];a_szam<<=8;
//			a_szam|=uRAM[A+2];a_szam<<=8;
//			a_szam|=uRAM[A+3];a_szam<<=8;
//			a_szam|=uRAM[A+4];a_szam<<=8;
//			a_szam|=uRAM[A+5];a_szam<<=8;
//			a_szam|=uRAM[A+6];a_szam<<=8;
//			a_szam|=uRAM[A+7];
//		}else{
//			a_szam=bswap64(*(unsigned long long*)&RAM[A]);
//		}
//		if(B&7){ // unaligned memory access detected. not needed on x86 or armv7+
//			b_szam=uRAM[B]<<8;
//			b_szam|=uRAM[B+1];b_szam<<=8;
//			b_szam|=uRAM[B+2];b_szam<<=8;
//			b_szam|=uRAM[B+3];b_szam<<=8;
//			b_szam|=uRAM[B+4];b_szam<<=8;
//			b_szam|=uRAM[B+5];b_szam<<=8;
//			b_szam|=uRAM[B+6];b_szam<<=8;
//			b_szam|=uRAM[B+7];
//		}else{
//			b_szam=bswap64(*(unsigned long long*)&RAM[B]);
//		}
//		// if(flog_entry>=logstart) fprintf(flog, "%lld %lld\n", a_szam, b_szam);
//		long long B_ertek=b_szam-a_szam;
//		// if(flog_entry>=logstart) fprintf(flog, "res %lld\n", B_ertek);fflush(flog);
//		if(B_ertek<=0){
//			eip=bswap32(utasitaskezd[5]);
//		}else{
//			eip+=24;
//		}
//		// if(flog_entry>=logstart) fprintf(flog, "neip:%d\n", eip);fflush(flog);
//		if(B&7){ // unaligned memory access detected. not needed on x86 or armv7+
//			b_szam=B_ertek; // __packed not works in shitty gcc
//			uRAM[B]=b_szam>>56;
//			uRAM[B+1]=b_szam>>48;
//			uRAM[B+2]=b_szam>>40;
//			uRAM[B+3]=b_szam>>32;
//			uRAM[B+4]=b_szam>>24;
//			uRAM[B+5]=b_szam>>16;
//			uRAM[B+6]=b_szam>>8;
//			uRAM[B+7]=b_szam;
//		}else{
//			*(long long*)&RAM[B]=bswap64(B_ertek);
//		}
//		// you may do more than one emulation cycle here, or you may use other optimization tricks.
//		// i dont know arm, so i will not do any spectacular here. you may do arm assembly (so you also can have big endian).
//	}
//	// flog_entry+=4096;
//	// if(flog_entry>logend) exit(0);
//	return eip;
//}

// SMP compatible subleq workaroud:
// you can skip this whole function if you dont need more cores.
// it manages the start, stop flags of cpu
// (and also updates the time)

void cpuemuciklus(long long id){
	// tell the OS if we have this cpu active (only needed for smp)
	unsigned long long int procireg=(id*16+PROCESSZOROK_ADAT);
	volatile unsigned long long int *r=(unsigned long long int *)&RAM[procireg];
	if(r[0]==0) r[0]=bswap64(4);
	int ures=0;
	while(1){
		if(id==0){ // update the TIME (delete if you dont need time)
		  unsigned long long t=(unsigned long long)((long double)jelenlegi_ido_s()*(long double)4294967296.0); // *4294967296.0
		  volatile unsigned long long *tt=(unsigned long long*)&RAM[IDO_ADAT];
		  tt[0]=bswap64(t);
		}

		// if cpu is being shutting down (needed only on smp systems)
		if(r[0]==bswap64(2)){
			r[0]=bswap64(4); // we acknowledged.
		}


		// go to SLEEP if nothing to do! (delete things related to ures and sleep if you dont need power saving)
		if(r[0]==bswap64(4)) ures++;
		if(ures>50000) ures=50000;
		if(ures>27000){
			usleep(1000);
		}else{
			if(ures>26000){
				usleep(100);
			}else{
				if(ures>25500){
					usleep(20);
				}else{
					if(ures==100) usleep(2);
					if(ures==200) usleep(3);
					if(ures==500) usleep(4);
					if(ures==5000) usleep(8);
					if(ures==20000) usleep(10);
				}
			}
		}


		if((r[0]==bswap64(1))||(id==0)){  // if the cpu is active, or we are on the 0th core)
			ures=0;
			// do the actual emulation, and save EIP
			// (if you dont want SMP, you dont have to save eip at all):
			r[1]=bswap64(emulalproc(bswap64(r[1])));
		}
		if(id==0) return;
	}
}


void * proc1emu(void *threadid){cpuemuciklus(1);}
void * proc2emu(void *threadid){cpuemuciklus(2);}
void * proc3emu(void *threadid){cpuemuciklus(3);}
void * proc4emu(void *threadid){cpuemuciklus(4);}
void * proc5emu(void *threadid){cpuemuciklus(5);}
void * proc6emu(void *threadid){cpuemuciklus(6);}
void * proc7emu(void *threadid){cpuemuciklus(7);}
void * proc8emu(void *threadid){cpuemuciklus(8);}
void * proc9emu(void *threadid){cpuemuciklus(9);}
void * proc10emu(void *threadid){cpuemuciklus(10);}
void * proc11emu(void *threadid){cpuemuciklus(11);}
void * proc12emu(void *threadid){cpuemuciklus(12);}

void emulalt_hardver_io(){
	threadstackmeretmax(64);
	STARTTHREAD(22, lemezciklus1);
	STARTTHREAD(41, lemezciklus2);

	long long wn=cpu_magok();
	if(wn>1) STARTTHREAD(26, proc1emu);
	if(wn>2) STARTTHREAD(27, proc2emu);
	if(wn>3) STARTTHREAD(28, proc3emu);
	if(wn>4) STARTTHREAD(29, proc4emu);
	if(wn>5) STARTTHREAD(30, proc5emu);
	if(wn>6) STARTTHREAD(31, proc6emu);
	if(wn>7) STARTTHREAD(32, proc7emu);
	if(wn>8) STARTTHREAD(33, proc8emu);
	if(wn>9) STARTTHREAD(34, proc9emu);
	if(wn>10) STARTTHREAD(35, proc10emu);
	if(wn>11) STARTTHREAD(36, proc11emu);
	if(wn>12) STARTTHREAD(37, proc12emu);
}

void glutpassive(int x, int y){
	valodi_egerkurzor_HelyeX=(float)x/RESX;
	valodi_egerkurzor_HelyeY=(float)y/RESY;
	float ex=valodi_egerkurzor_HelyeX;
	float ey=valodi_egerkurzor_HelyeY;
	volatile long long *adatcsomagptr=(long long *)(&RAM[EGER_ADAT]);
	if(adatcsomagptr[6]||adatcsomagptr[7]||adatcsomagptr[8]){ // a mouse signal is still pending...
		return;
	}

	// mouse X and Y (absolute)
	if((ex!=elozoegerxtmp)||(ey!=elozoegerytmp)){
		volatile long long *adatcsomagptr=(long long *)(&RAM[EGER_ADAT]);
		adatcsomagptr[6]=bswap64(ex*4294967296LL);
		elozoegerxtmp=ex;
		adatcsomagptr[7]=bswap64(ey*4294967296LL);
		elozoegerytmp=ey;
	}
}

void glutmouse(int button, int state, int x, int y){
	valodi_egerkurzor_HelyeX=(float)x/RESX;
	valodi_egerkurzor_HelyeY=(float)y/RESY;
	float ex=valodi_egerkurzor_HelyeX;
	float ey=valodi_egerkurzor_HelyeY;

	volatile long long *adatcsomagptr=(long long *)(&RAM[EGER_ADAT]);


	if(adatcsomagptr[6]||adatcsomagptr[7]||adatcsomagptr[8]){ // a mouse signal is still pending...
		for(int i=0;i<4096;i++) cpuemuciklus(0);
	}
	if(adatcsomagptr[6]||adatcsomagptr[7]||adatcsomagptr[8]){ // a mouse signal is still pending...
		for(int i=0;i<4096;i++) cpuemuciklus(0);
	}
	if(adatcsomagptr[6]||adatcsomagptr[7]||adatcsomagptr[8]){ // a mouse signal is still pending...
		for(int i=0;i<4096;i++) cpuemuciklus(0);
	}
	
	
	// mouse X and Y (absolute)
	if((ex!=elozoegerxtmp)||(ey!=elozoegerytmp)){
		volatile long long *adatcsomagptr=(long long *)(&RAM[EGER_ADAT]);
		adatcsomagptr[6]=bswap64(ex*4294967296LL);
		elozoegerxtmp=ex;
		adatcsomagptr[7]=bswap64(ey*4294967296LL);
		elozoegerytmp=ey;
	}

	
	if(button==GLUT_LEFT_BUTTON){
		if(state==GLUT_DOWN) adatcsomagptr[0]=bswap64(1);
		if(state==GLUT_UP) adatcsomagptr[0]=0;
	}
	if(button==GLUT_RIGHT_BUTTON){
		if(state==GLUT_DOWN) adatcsomagptr[1]=bswap64(1);
		if(state==GLUT_UP) adatcsomagptr[1]=0;
	}
	
	// glut middle button and wheel are fucked in most versions of glut
	// skipping it for now.
	
	//if(button==GLUT_MIDDLE_BUTTON){
	//	if(state==GLUT_DOWN) adatcsomagptr[2]=bswap64(1);
	//	if(state==GLUT_UP) adatcsomagptr[2]=0;
	//}
	// mouse wheel
	//if(egergorgofel){ // bswappelve ezt is
	//	long long egerhely=bswap64(adatcsomagptr[9]);
	//	egerhely-=egergorgofel;
	//	adatcsomagptr[9]=bswap64(egerhely);
	//	egergorgofel=0;
	//}
	//if(egergorgole){
	//	long long egerhely=bswap64(adatcsomagptr[9]);
	//	egerhely+=egergorgole;
	//	adatcsomagptr[9]=bswap64(egerhely);
	//	egergorgole=0;
	//}
	
}


void glutkey(unsigned char key, int x, int y){
	volatile long long *billentyuzet=(long long *)(&RAM[BILLENTYUZET_ADAT]);
	// be sure the os processed the previous keys.
	if(billentyuzet[0]){
		for(int i=0;i<4096;i++) cpuemuciklus(0);
	}
	if(billentyuzet[0]){
		for(int i=0;i<4096;i++) cpuemuciklus(0);
	}
	if(billentyuzet[0]){
		for(int i=0;i<4096;i++) cpuemuciklus(0);
	}
	if(key==127) key=29;
	billentyuzet[0]=bswap64(key);
}

void glutkey_spec(int key, int x, int y){
	volatile long long *billentyuzet=(long long *)(&RAM[BILLENTYUZET_ADAT]);
	// be sure the os processed the previous keys.
	if(billentyuzet[0]){
		for(int i=0;i<4096;i++) cpuemuciklus(0);
	}
	if(billentyuzet[0]){
		for(int i=0;i<4096;i++) cpuemuciklus(0);
	}
	if(billentyuzet[0]){
		for(int i=0;i<4096;i++) cpuemuciklus(0);
	}
	if(key==GLUT_KEY_UP) key=14;
	if(key==GLUT_KEY_DOWN) key=16;
	if(key==GLUT_KEY_LEFT) key=15;
	if(key==GLUT_KEY_RIGHT) key=17;
	if(key==GLUT_KEY_HOME) key=2;
	if(key==GLUT_KEY_END) key=3;
	billentyuzet[0]=bswap64(key);
}

void glutcycle(){
	for(int i=0;i<4096;i++) cpuemuciklus(0);
	munkaciklus();
}

// int main(){
int main(int argc, char *argv[]){
	// setting up 384 mbyte of RAM
	RAM=(unsigned char*)malloc(384*1024*1024);
	memset(RAM, 0, 384*1024*1024); // dawn expects legit memory areas to zero
	// you should add some memory garbage to the last few kbyte if you dont want the emulator to crash if OS exceeds the memory size:
	memset(&RAM[383*1024*1024], 255, 1*1024*1024);

	// getting up cpu name
	rtCPUID();
	strcpy((char*)&RAM[PROCESSZORNEVE], cpunevoprhez);

	// we giving a hint to the kernel that it should do around 20k clock execution window in the scheduler, so the kernel will not rapidly turn them on/off wildly.
	// this is not necessary, but without this, SMP systems will may have a responsibility impact, as OS will assume there is no wait time.
	unsigned long long int *mennyiproc=(unsigned long long int *)&RAM[MENNYI_ORAJEL_PROCIAKTIVALAS];
	mennyiproc[0]=bswap64(20000);

	// loading the os (first 256 mbyte)
	#ifdef WIN32
		FILE * f=fopen("..\\..\\disk0.bin", "rb");
	#else
		FILE * f=fopen("../../disk0.bin", "rb");
	#endif
	if(f){
		fread(RAM, 1, 256*1024*1024, f);
		fclose(f);
		f=NULL;
	}else{
		printf("disk ..\\..\\disk0.bin not found.\n");
		exit(0);
	}

	// yes, we are a real cpu (sort of)
	unsigned long long int procireg=(0*16+PROCESSZOROK_ADAT);
	volatile unsigned long long int *r=(unsigned long long int *)&RAM[procireg];
	r[0]=bswap64(1);

	// starting disk emulation thread and cpu core threads
	emulalt_hardver_io();

	// initializing opengl es in the emulator for the display
	int vn=1;
	char valami[8];
	valami[0]='a';
	valami[1]=0;
	char *vvv[2];
	vvv[0]=valami;
	glutInit(&vn, vvv);
	//glutInit(&argc, argv);	

	
	glutInitDisplayMode(GLUT_RGB);
	glutInitWindowSize(RESX,RESY);
	glutCreateWindow("Dawn emulator");

	volatile long long *vga=(long long *)(&RAM[FELBONTAS_ADAT]);
	vga[0]=bswap64(RESX); // in case the os want different resolution... it wont get it, coz why, we forcing the os to the window screen
	vga[1]=bswap64(RESY);
	vga[2]=bswap64(32); // 32 bit
	vga[3]=bswap64(2); // tell the os we having direct frame buffer support (so the OS can render 2x faster)
	
	// callback based magic cancer of gluthhh
	glutDisplayFunc(glutcycle);
	glutKeyboardFunc(glutkey);
	glutSpecialFunc(glutkey_spec);
	glutMouseFunc(glutmouse);
	glutIdleFunc(glutcycle);
	glutMotionFunc(glutpassive);
	glutPassiveMotionFunc(glutpassive);
	
	glutMainLoop();
	
}

